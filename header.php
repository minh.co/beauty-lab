<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="<?php bloginfo( 'charset' ); ?>" />
    <!-- font family -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
      rel="stylesheet"
      href="https://unpkg.com/swiper@8/swiper-bundle.min.css"
    />
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <?php wp_head(); ?>
    
    <!-- Load WP objects for head-tag -->
</head>

<body >
    <header class="container-fluid Header">
        <ul class="logo">
            <li>
                <img width="43" height="64" src="<?php bloginfo('template_directory'); ?>/assets/images/logo.png" alt="logo" />
            </li>
            <li>
                <img width="91" height="64" src="<?php bloginfo('template_directory'); ?>/assets/images/brandname.png" alt="logo" />
            </li>
        </ul>
        <?php
            wp_nav_menu(array(
                'theme_location' => 'headerMenu',
            ));
        ?>
        <?php
            wp_nav_menu(array(
                'theme_location' => 'headerUser',
            ));
        ?>
    </header>