import { registerBlockType } from "@wordpress/blocks";
import {
  InspectorControls,
  useBlockProps,
  useInnerBlocksProps,
  MediaUpload,
  MediaUploadCheck,
  __experimentalUseColorProps as useColorProps,
  __experimentalGetColorClassesAndStyles as getColorClassesAndStyles,
} from "@wordpress/block-editor";
import { LollypopLogo } from "../../../assets/images/logo";
import {
  Button,
  PanelBody,
  PanelRow,
  __experimentalUnitControl as UnitControl,
  TabPanel,
  ToggleControl,
  __experimentalAlignmentMatrixControl as AlignmentMatrixControl,
} from "@wordpress/components";

registerBlockType("block-themes/container-template", {
  apiVersion: 2,
  title: "Container Template",
  category: "lollypop",
  icon: {
    src: LollypopLogo,
  },
  attributes: {
    color: { type: "string", default: "transparent" },
    backgroundImage: {
      type: "object",
      url: "",
    },
    paddingY: { type: "string" },
    marginY: { type: "string" },
    fullScreen: { type: "boolean", default: false },
    repeat: { type: "boolean", default: false },
    cover: { type: "boolean", default: true },
    position: { type: "string", default: "center" },
    customClassName: { type: "string", default: "" },
    customStyle: { type: "object" },
  },
  supports: {
    className: false,
    color: {
      gradients: true,
      __experimentalSkipSerialization: true,
      __experimentalDefaultControls: {
        background: true,
        gradients: true,
        text: false,
      },
    },
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent({ attributes, setAttributes = () => {} }) {
  const {
    color,
    backgroundImage: { url } = {},
    paddingY,
    marginY,
    fullScreen,
    repeat,
    cover,
    position,
    customClassName,
    customStyle,
  } = attributes;

  const { style, className } = useColorProps(attributes);

  const blockProps = useBlockProps({
    className: `${
      fullScreen ? "container-fluid" : "container"
    } ${className} ${customClassName}`,
    style: {
      backgroundColor: color,
      backgroundImage: `url('${url}')`,
      backgroundRepeat: `${repeat ? "repeat" : "no-repeat"}`,
      backgroundSize: `${cover ? "cover" : "contain"}`,
      backgroundPosition: position,
      padding: `${paddingY} 0`,
      marginTop: `${marginY}`,
      marginBottom: `${marginY}`,
      ...style,
      ...customStyle,
    },
  });

  const innerBlocksProps = useInnerBlocksProps(blockProps);

  const handleFileSelect = (file) => {
    setAttributes({
      backgroundImage: { url: file.url },
      height: `${file.height}px`,
    });
  };

  const handleClearFile = () => {
    setAttributes({ backgroundImage: { url: null } });
  };

  return (
    <>
      <InspectorControls>
        <TabPanel
          className="my-tab-panel"
          activeClass="active-tab"
          orientation="horizontal"
          initialTabName="tab1"
          tabs={[
            {
              name: "tab1",
              title: "Basic style",
              className: "tab-one",
            },
            {
              name: "tab2",
              title: "Background style",
              className: "tab-two",
            },
          ]}
        >
          {(tab) => (
            <>
              {tab.name === "tab1" && (
                <>
                  <PanelBody title="Box Style" initialOpen={true}>
                    <PanelRow>
                      <UnitControl
                        value={paddingY}
                        isPressEnterToChange
                        labelPosition="top"
                        label="Padding Axis Y"
                        type="string"
                        onChange={(paddingY) => setAttributes({ paddingY })}
                      />
                    </PanelRow>
                    <PanelRow>
                      <UnitControl
                        value={marginY}
                        isPressEnterToChange
                        labelPosition="top"
                        label="Margin Axis Y"
                        type="tring"
                        onChange={(marginY) => setAttributes({ marginY })}
                      />
                    </PanelRow>
                  </PanelBody>
                </>
              )}
              {tab.name === "tab2" && (
                <>
                  <PanelBody title="Background Image" initialOpen={true}>
                    <PanelRow>
                      <MediaUploadCheck>
                        <MediaUpload
                          onSelect={handleFileSelect}
                          value={1}
                          render={({ open }) => {
                            return (
                              <Button variant="primary" onClick={open}>
                                Change Picture
                              </Button>
                            );
                          }}
                        />
                      </MediaUploadCheck>
                      <Button variant="secondary" onClick={handleClearFile}>
                        Clear picture
                      </Button>
                    </PanelRow>
                  </PanelBody>
                  <PanelBody title="Screen style" initialOpen={true}>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        height: "fit-content",
                      }}
                    >
                      <div style={{ width: "30%" }}>
                        <ToggleControl
                          checked={fullScreen}
                          help={fullScreen ? "Full screen" : "Fit screen"}
                          onChange={(fullScreen) =>
                            setAttributes({ fullScreen })
                          }
                        />
                      </div>
                      <div style={{ width: "30%" }}>
                        <ToggleControl
                          checked={repeat}
                          help={repeat ? "Repeat" : "No repeat"}
                          onChange={(repeat) => setAttributes({ repeat })}
                        />
                      </div>
                      <div style={{ width: "30%" }}>
                        <ToggleControl
                          checked={cover}
                          help={cover ? "Image Cover" : "Image Contain"}
                          onChange={(cover) => setAttributes({ cover })}
                        />
                      </div>
                    </div>
                  </PanelBody>
                  <PanelBody title="Background Position">
                    <AlignmentMatrixControl
                      value={position}
                      onChange={(position) => setAttributes({ position })}
                    />
                  </PanelBody>
                </>
              )}
            </>
          )}
        </TabPanel>
      </InspectorControls>
      <div {...innerBlocksProps}></div>
    </>
  );
}

function SaveComponent({ attributes }) {
  const {
    color,
    backgroundImage: { url } = {},
    paddingY,
    marginY,
    fullScreen,
    repeat,
    cover,
    position,
    customClassName,
    customStyle,
  } = attributes;

  const { className, style } = getColorClassesAndStyles(attributes);

  const innerBlocksProps = useInnerBlocksProps.save(
    useBlockProps.save({
      className: `${
        fullScreen ? "container-fluid" : "container"
      } ${className} ${customClassName}`,
      style: {
        backgroundColor: color,
        backgroundImage: `url('${url}')`,
        backgroundRepeat: `${repeat ? "repeat" : "no-repeat"}`,
        backgroundSize: `${cover ? "cover" : "contain"}`,
        backgroundPosition: position,
        padding: `${paddingY} 0`,
        marginTop: `${marginY}`,
        marginBottom: `${marginY}`,
        ...style,
        ...customStyle,
      },
    })
  );

  return <div {...innerBlocksProps}></div>;
}
