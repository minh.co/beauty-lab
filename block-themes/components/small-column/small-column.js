import { registerBlockType } from "@wordpress/blocks";
import { BlockControls, InnerBlocks, RichText } from "@wordpress/block-editor";
import { LollypopLogo } from "../../../assets/images/logo";

registerBlockType("block-themes/small-column", {
  title: "Small Column",
  category: "lollypop",
  icon: LollypopLogo,
  attributes: {
    title: { type: "string" },
    subTitle: { type: "string" },
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent() {
  const template = [
    ["core/heading", { placeholder: "Title" }],
    ["core/paragraph", { placeholder: "Subtitle" }],
  ];
  return (
    <li className="smallColumn">
      <InnerBlocks template={template} />
    </li>
  );
}

function SaveComponent() {
  return (
    <li className="smallColumn">
      <InnerBlocks.Content />
    </li>
  );
}
