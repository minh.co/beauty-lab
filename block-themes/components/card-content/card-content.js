import { registerBlockType } from "@wordpress/blocks";
import {
  InspectorControls,
  useBlockProps,
  useInnerBlocksProps,
  BlockControls,
  AlignmentToolbar,
  __experimentalUseColorProps as useColorProps,
  __experimentalGetColorClassesAndStyles as getColorClassesAndStyles,
} from "@wordpress/block-editor";
import {
  PanelBody,
  PanelRow,
  ColorPicker,
  __experimentalUnitControl as UnitControl,
  __experimentalBoxControl as BoxControl,
  ToolbarGroup,
  TabPanel,
  RangeControl,
  RadioControl,
} from "@wordpress/components";
import { LollypopLogo } from "../../../assets/images/logo";
import { SolidLine, DashedLine, DottedLine } from "../../../assets/images/icon";

registerBlockType("block-themes/card-content", {
  apiVersion: 2,
  title: "Card Content",
  category: "lollypop",
  icon: {
    src: LollypopLogo,
  },
  attributes: {
    padding: {
      type: "object",
      default: { top: "0", right: "0", bottom: "0", left: "0" },
    },
    margin: {
      type: "object",
      default: { top: "0", right: "0", bottom: "0", left: "0" },
    },
    width: { type: "string" },
    height: { type: "string" },
    borderRadius: { type: "number", default: 0 },
    borderWidth: {
      type: "object",
      default: { top: "0", right: "0", bottom: "0", left: "0" },
    },
    borderColor: { type: "string", default: "#000000" },
    borderStyle: { type: "string", default: "solid" },
    align: {
      type: "string",
      default: "center",
    },
    customClassName: { type: "string", default: "" },
    customStyle: { type: "object" },
  },
  supports: {
    className: false,
    color: {
      gradients: true,
      __experimentalSkipSerialization: true,
      __experimentalDefaultControls: {
        background: true,
        text: true,
        gradients: true,
      },
    },
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent(props) {
  const { attributes = {}, setAttributes = () => {} } = props;

  const {
    padding,
    margin,
    width,
    height,
    align,
    borderRadius,
    borderWidth,
    borderColor,
    borderStyle,
    customClassName,
    customStyle,
  } = attributes;

  const { style, className } = useColorProps(attributes);

  const blockProps = useBlockProps({
    className: `card-content border-${borderStyle} ${className} text-${align} ${customClassName}`,
    style: {
      width: width,
      height: height,
      padding: `${padding.top} ${padding.right} ${padding.bottom} ${padding.left}`,
      margin: `${margin.top} ${margin.right} ${margin.bottom} ${margin.left}`,
      borderColor: borderColor,
      borderTop: borderWidth.top,
      borderRight: borderWidth.right,
      borderBottom: borderWidth.bottom,
      borderLeft: borderWidth.left,
      borderRadius: `${borderRadius}px`,
      ...style,
      ...customStyle,
    },
  });

  const innerBlocksProps = useInnerBlocksProps(blockProps, {});

  const handleCheck = (type, { top, right, bottom, left }) => {
    if (type === "padding") {
      setAttributes({
        padding: {
          top: top || "0",
          right: right || "0",
          bottom: bottom || "0",
          left: left || "0",
        },
      });
    }
    if (type === "margin") {
      setAttributes({
        margin: {
          top: top || "0",
          right: right || "0",
          bottom: bottom || "0",
          left: left || "0",
        },
      });
    }
    if (type === "borderWidth") {
      setAttributes({
        borderWidth: {
          top: top || "0",
          right: right || "0",
          bottom: bottom || "0",
          left: left || "0",
        },
      });
    }
  };

  return (
    <>
      <BlockControls>
        <ToolbarGroup>
          <AlignmentToolbar
            value={align}
            alignmentControls={[
              {
                title: "Left",
                icon: "editor-alignleft",
                align: "left",
              },
              {
                title: "Center",
                icon: "editor-aligncenter",
                align: "center",
              },
              {
                title: "Right",
                icon: "editor-alignright",
                align: "right",
              },
            ]}
            onChange={(align) => setAttributes({ align })}
          />
        </ToolbarGroup>
      </BlockControls>
      <InspectorControls>
        <TabPanel
          className="my-tab-panel"
          activeClass="active-tab"
          orientation="horizontal"
          initialTabName="tab1"
          tabs={[
            {
              name: "tab1",
              title: "Basic style",
              className: "tab-one",
            },
            {
              name: "tab2",
              title: "Border style",
              className: "tab-two",
            },
          ]}
        >
          {(tab) => (
            <>
              {tab.name === "tab1" && (
                <>
                  <PanelBody title="Size" initialOpen={true}>
                    <PanelRow>
                      <div style={{ width: "40%" }}>
                        <UnitControl
                          value={width}
                          isPressEnterToChange
                          labelPosition="top"
                          label="Width"
                          type="string"
                          onChange={(width) => setAttributes({ width })}
                        />
                      </div>
                      <div style={{ width: "40%" }}>
                        <UnitControl
                          value={height}
                          isPressEnterToChange
                          labelPosition="top"
                          label="Height"
                          type="tring"
                          onChange={(height) => setAttributes({ height })}
                        />
                      </div>
                    </PanelRow>
                  </PanelBody>
                  <PanelBody title="Padding" initialOpen={true}>
                    <PanelRow>
                      <BoxControl
                        onChange={(value) => handleCheck("padding", value)}
                        defaultValues={{
                          top: "0px",
                          right: "0px",
                          bottom: "0px",
                          left: "0px",
                        }}
                      />
                    </PanelRow>
                  </PanelBody>
                  <PanelBody title="Margin" initialOpen={true}>
                    <PanelRow>
                      <BoxControl
                        onChange={(value) => handleCheck("margin", value)}
                        defaultValues={{
                          top: "0px",
                          right: "0px",
                          bottom: "0px",
                          left: "0px",
                        }}
                      />
                    </PanelRow>
                  </PanelBody>
                </>
              )}
              {tab.name === "tab2" && (
                <>
                  <PanelBody title="Border Width" initialOpen={true}>
                    <PanelRow>
                      <BoxControl
                        label={false}
                        onChange={(value) => handleCheck("borderWidth", value)}
                        defaultValues={{
                          top: "0px",
                          right: "0px",
                          bottom: "0px",
                          left: "0px",
                        }}
                      />
                    </PanelRow>
                  </PanelBody>
                  <PanelBody title="Border Color" initialOpen={true}>
                    <PanelRow>
                      <ColorPicker
                        color={borderColor}
                        onChangeComplete={({ hex }) =>
                          setAttributes({ borderColor: hex })
                        }
                      />
                    </PanelRow>
                  </PanelBody>
                  <PanelBody title="Border style" initialOpen={true}>
                    <PanelRow>
                      <RadioControl
                        onChange={(borderStyle) =>
                          setAttributes({ borderStyle })
                        }
                        options={[
                          {
                            label: "None Border",
                            value: "none",
                          },
                          {
                            label: <SolidLine fill={borderColor} />,
                            value: "solid",
                          },
                          {
                            label: <DashedLine fill={borderColor} />,
                            value: "dashed",
                          },
                          {
                            label: <DottedLine fill={borderColor} />,
                            value: "dotted",
                          },
                        ]}
                        selected={borderStyle}
                      />
                    </PanelRow>
                  </PanelBody>

                  <PanelBody title="Border Radius" initialOpen={true}>
                    <RangeControl
                      help="Change card border to circle"
                      initialPosition={0}
                      max={100}
                      min={0}
                      onChange={(borderRadius) =>
                        setAttributes({ borderRadius })
                      }
                      currentInput={0}
                    />
                  </PanelBody>
                </>
              )}
            </>
          )}
        </TabPanel>
      </InspectorControls>

      <div {...innerBlocksProps}></div>
    </>
  );
}

function SaveComponent({ attributes }) {
  const {
    padding,
    margin,
    width,
    height,
    align,
    borderRadius,
    borderWidth,
    borderColor,
    borderStyle,
    customClassName,
    customStyle,
  } = attributes;

  const { className, style } = getColorClassesAndStyles(attributes);

  const innerBlocksProps = useInnerBlocksProps.save(
    useBlockProps.save({
      className: `card-content border-${borderStyle} ${className} text-${align} ${customClassName}`,
      style: {
        width: width,
        height: height,
        padding: `${padding.top} ${padding.right} ${padding.bottom} ${padding.left}`,
        margin: `${margin.top} ${margin.right} ${margin.bottom} ${margin.left}`,
        borderColor: borderColor,
        borderTop: borderWidth.top,
        borderRight: borderWidth.right,
        borderBottom: borderWidth.bottom,
        borderLeft: borderWidth.left,
        borderRadius: `${borderRadius}px`,
        ...style,
        ...customStyle,
      },
    })
  );

  return <div {...innerBlocksProps}></div>;
}
