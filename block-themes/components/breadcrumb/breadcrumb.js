import { registerBlockType, registerBlockStyle } from "@wordpress/blocks";
import apiFetch from "@wordpress/api-fetch";
import {
  InspectorControls,
  useBlockProps,
  useInnerBlocksProps,
  __experimentalGetColorClassesAndStyles as getColorClassesAndStyles,
  MediaUpload,
  MediaUploadCheck,
} from "@wordpress/block-editor";
import { PanelBody, PanelRow, Button, Icon } from "@wordpress/components";
import { plusCircle } from "@wordpress/icons";
import { useSelect } from "@wordpress/data";
import { useEffect } from "react";

import { ConSonLogo } from "../../../assets/images/logoConSon";

registerBlockType("block-themes/breadcrumb", {
  apiVersion: 2,
  title: "Con Son Breadcrumb",
  category: "conson",
  icon: { src: ConSonLogo },
  html: false,
  attributes: {
    background: { type: "string", default: BG },
    link: { type: "string" },
    bread: { type: "array", default: [] },
  },
  // get post Id and postType in store context
  usesContext: ["postId","postType"],
  supports: {
    // align: ["left", "right", "center"],
    color: {
      __experimentalSkipSerialization: true,
      gradients: true,
      __experimentalDefaultControls: {
        background: true,
        text: true,
      },
    },
  },

  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent({ attributes = {}, setAttributes = () => {}, context }) {
  console.log(context);
  const { bread } = attributes;
  const { postId, postType } = context;


  const fetchPosts = async () => {
   
    const response = await apiFetch({
      path: `/wp/v2/${postType}s/${postId}`,
      method: "GET",
    });
    console.log(response);
    let breadcrumb = response.link;
    // Remove word wordpress if exist
    if (response.link.includes("/wordpress")) {
      let data = breadcrumb.split("/wordpress").join("");
      setAttributes({ link: data });
      // Take url from position 3 to the end
      let array = data.split("/").slice(3);
      // Add Home in top array
      array.unshift("Home");
      console.log(array);
      setAttributes({ bread: array });
    }

  };
  useEffect(() => {
    fetchPosts();
  }, []);

  const renderBread = (
    <>
      {bread?.map((val, index) => {
        let page = val.replace(/-/g, " ");
        let capitalize = page.replace(/\b\w/g, (l) => l.toUpperCase());
        if (val === '') {
          return (
            <span>
              <a href="">{capitalize}</a>
            </span>
          );
        } else {
          return (
            <span>
              <a href="">{capitalize}</a> <img src={arrow} alt="arrow"/>
            </span>
          );
        }
      })}
    </>
  );


  return (
    <>
     <div className="breadcrumb">{renderBread}</div>
    
    </>
  );
}

function SaveComponent({ attributes = {} }) {
  const { link, bread } = attributes;
  console.log(link);



  const renderBread = (
    <>
      {bread?.map((val, index) => {
        let page = val.replace(/-/g, " ");
        let capitalize = page.replace(/\b\w/g, (l) => l.toUpperCase());
        if (index === bread.length - 1) {
          return (
            <span>
              <a href="">{capitalize}</a>
            </span>
          );
        } else {
          return (
            <span>
              <a href="">{capitalize}</a> <img src={arrow} alt="arrow"/>
            </span>
          );
        }
      })}
    </>
  );

  return (
    <>
      <div className="breadcrumb">{renderBread}</div>
    </>
  );
}
