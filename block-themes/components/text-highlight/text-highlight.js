import { registerBlockType } from "@wordpress/blocks";
import {
  InspectorControls,
  getColorObjectByColorValue,
  useBlockProps,
  useInnerBlocksProps,
} from "@wordpress/block-editor";
import {
  PanelBody,
  PanelRow,
  ColorPalette,
  __experimentalNumberControl as NumberControl,
} from "@wordpress/components";
import { LollypopLogo } from "../../../assets/images/logo";

registerBlockType("block-themes/text-highlight", {
  apiVersion: 2,
  title: "Text Highlight",
  category: "lollypop",
  icon: LollypopLogo,
  attributes: {
    colorCode: { type: "string", default: "red" },
    colorName: { type: "string", default: "" },
  },
  supports: {
    align: ["left", "right", "center"],
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent(props) {
  // const blockProps = useBlockProps();

  const { attributes = {}, setAttributes = () => {} } = props;
  const { colorCode, colorName } = attributes;
  const blockProps = useBlockProps({ className: "text-highlight" });
  const ALLOWED_BLOCKS = ["core/paragraph"];

  const MY_TEMPLATE = [["core/paragraph", { placeholder: "Link..." }]];
  const innerBlocksProps = useInnerBlocksProps(blockProps, {
    allowedBlocks: ALLOWED_BLOCKS,
    template: MY_TEMPLATE,
  });
  const ourColors = [
    { name: "primary", color: "#FD2E35" },
    { name: "dark", color: "#221429" },
  ];

  function handleColorChange(colorCode) {
    const { name } = getColorObjectByColorValue(ourColors, colorCode);
    setAttributes({ colorName: name });
    setAttributes({ colorCode });
  }
  return (
    <>
      <InspectorControls>
        <PanelBody title="Color Highlight" initialOpen={true}>
          <PanelRow>
            <ColorPalette
              colors={ourColors}
              value={colorCode}
              onChange={(colorCode) => handleColorChange(colorCode)}
            />
          </PanelRow>
        </PanelBody>
      </InspectorControls>
      <div
        {...innerBlocksProps}
        style={{ "--tooltip-color": colorCode }}
        data-line={colorCode}
        data-line-name={colorName}
      ></div>
    </>
  );
}

function SaveComponent({ attributes = {}, setAttributes = () => {} }) {
  const { colorCode, colorName } = attributes;
  const innerBlocksProps = useInnerBlocksProps.save(useBlockProps.save());

  return (
    <div
      {...innerBlocksProps}
      style={{ "--tooltip-color": colorCode }}
      className={"text-highlight"}
      data-line={colorCode}
      data-line-name={colorName}
    ></div>
  );
}
