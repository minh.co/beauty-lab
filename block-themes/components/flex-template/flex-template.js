import { registerBlockType } from "@wordpress/blocks";
import {
  InspectorControls,
  useBlockProps,
  useInnerBlocksProps,
} from "@wordpress/block-editor";
import { LollypopLogo } from "../../../assets/images/logo";
import {
  PanelBody,
  PanelRow,
  __experimentalUnitControl as UnitControl,
  __experimentalBoxControl as BoxControl,
  TabPanel,
  RadioControl,
} from "@wordpress/components";

registerBlockType("block-themes/flex-template", {
  apiVersion: 2,
  title: "Flex Template",
  category: "lollypop",
  icon: {
    src: LollypopLogo,
  },
  attributes: {
    justify: { type: "string", default: "start" },
    alignItems: { type: "string", default: "start" },
    alignContent: { type: "string", default: "start" },
    direction: { type: "string", default: "row" },
    flexWrap: { type: "string", default: "wrap" },
    padding: {
      type: "object",
      default: { top: "0", right: "0", bottom: "0", left: "0" },
    },
    margin: {
      type: "object",
      default: { top: "0", right: "0", bottom: "0", left: "0" },
    },
    customClassName: { type: "string", default: "" },
    customStyle: { type: "object" },
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent({ attributes, setAttributes = () => {} }) {
  const {
    justify,
    alignItems,
    alignContent,
    direction,
    flexWrap,
    padding,
    margin,
    customClassName,
    customStyle,
  } = attributes;

  const blockProps = useBlockProps({
    className: `d-flex Flex-template justify-content-${justify} align-items-${alignItems} align-content-${alignContent} flex-${direction} flex-${flexWrap} ${customClassName}`,
    style: {
      padding: `${padding.top} ${padding.right} ${padding.bottom} ${padding.left}`,
      margin: `${margin.top} ${margin.right} ${margin.bottom} ${margin.left}`,
      ...customStyle,
    },
  });

  const innerBlocksProps = useInnerBlocksProps(blockProps);

  const handleCheck = (type, { top, right, bottom, left }) => {
    if (type === "padding") {
      setAttributes({
        padding: {
          top: top || "0",
          right: right || "0",
          bottom: bottom || "0",
          left: left || "0",
        },
      });
    }
    if (type === "margin") {
      setAttributes({
        margin: {
          top: top || "0",
          right: right || "0",
          bottom: bottom || "0",
          left: left || "0",
        },
      });
    }
  };

  return (
    <>
      <InspectorControls>
        <TabPanel
          className="my-tab-panel"
          activeClass="active-tab"
          orientation="horizontal"
          initialTabName="tab2"
          tabs={[
            {
              name: "tab1",
              title: "Alignment Items style",
              className: "tab-one",
            },
            {
              name: "tab2",
              title: "Box content style",
              className: "tab-two",
            },
          ]}
        >
          {(tab) => (
            <>
            {tab.name === "tab1" && (
                <>
                  <PanelBody title="Justify items" initialOpen={true}>
                    <PanelRow>
                      <div style={{ width: "35%" }}>
                        <RadioControl
                          onChange={(justify) => setAttributes({ justify })}
                          options={[
                            {
                              label: "Left",
                              value: "start",
                            },
                            {
                              label: "Center",
                              value: "center",
                            },
                            {
                              label: "Right",
                              value: "end",
                            },
                          ]}
                          selected={justify}
                        />
                      </div>
                      <div style={{ width: "65%" }}>
                        <RadioControl
                          onChange={(justify) => setAttributes({ justify })}
                          options={[
                            {
                              label: "Space Around",
                              value: "around",
                            },
                            {
                              label: "Space Between",
                              value: "between",
                            },
                            {
                              label: "Evenly",
                              value: "evenly",
                            },
                          ]}
                          selected={justify}
                        />
                      </div>
                    </PanelRow>
                  </PanelBody>
                  <PanelBody title="Alignment Items" initialOpen={true}>
                    <div style={{ display: "flex", alignItems: "flex-start" }}>
                      <div style={{ width: "45%" }}>
                        <RadioControl
                          onChange={(alignItems) =>
                            setAttributes({ alignItems })
                          }
                          options={[
                            {
                              label: "Start",
                              value: "start",
                            },
                            {
                              label: "Center",
                              value: "center",
                            },
                            {
                              label: "End",
                              value: "end",
                            },
                          ]}
                          selected={alignItems}
                        />
                      </div>
                      <div style={{ width: "55%" }}>
                        <RadioControl
                          onChange={(alignItems) =>
                            setAttributes({ alignItems })
                          }
                          options={[
                            {
                              label: "Base Line",
                              value: "baseline",
                            },
                            {
                              label: "Stretch",
                              value: "stretch",
                            },
                          ]}
                          selected={alignItems}
                        />
                      </div>
                    </div>
                  </PanelBody>
                  <PanelBody title="Direction" initialOpen={true}>
                    <PanelRow>
                      <RadioControl
                        onChange={(direction) => setAttributes({ direction })}
                        options={[
                          {
                            label: "Row",
                            value: "row",
                          },
                          {
                            label: "Row Reverse",
                            value: "row-reverse",
                          },
                          {
                            label: "Column",
                            value: "column",
                          },
                          {
                            label: "Column Reverse",
                            value: "column-reverse",
                          },
                        ]}
                        selected={direction}
                      />
                    </PanelRow>
                  </PanelBody>

                  <PanelBody
                    title="Border Radius"
                    initialOpen={true}
                  ></PanelBody>
                </>
              )}
              {tab.name === "tab2" && (
                <>
                  <PanelBody title="Flex Wrap" initialOpen={true}>
                    <RadioControl
                      help="This property has no effect on single rows"
                      onChange={(flexWrap) => setAttributes({ flexWrap })}
                      options={[
                        {
                          label: "Wrap",
                          value: "wrap",
                        },
                        {
                          label: "Wrap Reserve",
                          value: "wrap-reverse",
                        },
                        {
                          label: "No Wrap",
                          value: "nowrap",
                        },
                      ]}
                      selected={flexWrap}
                    />
                  </PanelBody>
                  <PanelBody title="Content Alignment" initialOpen={true}>
                    <RadioControl
                      help="This property has no effect on single rows"
                      onChange={(alignContent) =>
                        setAttributes({ alignContent })
                      }
                      options={[
                        {
                          label: "Start",
                          value: "start",
                        },
                        {
                          label: "Center",
                          value: "center",
                        },
                        {
                          label: "End",
                          value: "end",
                        },
                        {
                          label: "Space Between",
                          value: "between",
                        },
                        {
                          label: "Space Around",
                          value: "around",
                        },
                        {
                          label: "Stretch",
                          value: "stretch",
                        },
                      ]}
                      selected={alignContent}
                    />
                  </PanelBody>
                  <PanelBody title="Padding" initialOpen={true}>
                    <PanelRow>
                      <BoxControl
                        onChange={(value) => handleCheck("padding", value)}
                        defaultValues={{
                          top: "0px",
                          right: "0px",
                          bottom: "0px",
                          left: "0px",
                        }}
                      />
                    </PanelRow>
                  </PanelBody>
                  <PanelBody title="Margin" initialOpen={true}>
                    <PanelRow>
                      <BoxControl
                        onChange={(value) => handleCheck("margin", value)}
                        defaultValues={{
                          top: "0px",
                          right: "0px",
                          bottom: "0px",
                          left: "0px",
                        }}
                      />
                    </PanelRow>
                  </PanelBody>
                </>
              )}
              
            </>
          )}
        </TabPanel>
      </InspectorControls>
      <div {...innerBlocksProps}></div>
    </>
  );
}

function SaveComponent({ attributes }) {
  const {
    justify,
    alignItems,
    alignContent,
    direction,
    flexWrap,
    padding,
    margin,
    customClassName,
    customStyle,
  } = attributes;

  const innerBlocksProps = useInnerBlocksProps.save(
    useBlockProps.save({
      className: `d-flex Flex-template justify-content-${justify} align-items-${alignItems} align-content-${alignContent} flex-${direction} flex-${flexWrap} ${customClassName}`,
      style: {
        padding: `${padding.top} ${padding.right} ${padding.bottom} ${padding.left}`,
        margin: `${margin.top} ${margin.right} ${margin.bottom} ${margin.left}`,
        ...customStyle,
      },
    })
  );

  return <div {...innerBlocksProps}></div>;
}
