import { registerBlockType, registerBlockStyle } from "@wordpress/blocks";
import {
  InspectorControls,
  useBlockProps,
  __experimentalGetColorClassesAndStyles as getColorClassesAndStyles,
  __experimentalUseColorProps as useColorProps,
} from "@wordpress/block-editor";
import {
  PanelBody,
  PanelRow,
  RangeControl,
  __experimentalBoxControl as BoxControl,
  ButtonGroup,
  Button,
} from "@wordpress/components";
import { useState } from "react";

import { ConSonLogo } from "../../../assets/images/logoConSon";

registerBlockType("block-themes/input", {
  $schema: "https://schemas.wp.org/trunk/block.json",
  apiVersion: 2,
  title: "Input",
  category: "conson",
  icon: { src: ConSonLogo },
  html: false,
  attributes: {
    typeInput: { type: "string", default: "text" },
    placeholder: { type: "string", default: "Enter text..." },
    borderRadius: { type: "number", default: 0 },
    padding: {
      type: "object",
      default: { top: "0", right: "0", bottom: "0", left: "0" },
    },
    value:{type:"string"},
    width: { type: "string", default: "auto" },
  },
  styles: [
    { name: "border", label: "With Border", isDefault: true },
    { name: "border-none", label: "None" },
  ],
  supports: {
    // align: ["left", "right", "center"],
    color: {
      __experimentalSkipSerialization: true,
      gradients: true,
      __experimentalDefaultControls: {
        background: true,
        text: true,
      },
    },
    typography: {
			fontSize: true,
			__experimentalFontFamily: true,
			__experimentalTextTransform: true,
			__experimentalDefaultControls: {
				fontSize: true
			}
		},
  	__experimentalBorder: {
			radius: true,
			__experimentalSkipSerialization: true,
			__experimentalDefaultControls: {
				radius: true
			}
		},
  },
  example: {
    typeInput: "text",
    placeholder: "Enter text...",
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent({ attributes = {}, setAttributes = () => {} }) {
  const { typeInput, placeholder, borderRadius, padding,width, value } = attributes;
  const [isPrimary, setIsPrimary] = useState([
    { status: true, value: "auto" },
    { status: false, value: "25%" },
    { status: false, value: "50%" },
    { status: false, value: "100%" },
  ]);
  const blockProps = useBlockProps({ className: "input-control" });
  const colorProps = useColorProps(attributes);

  const handleCheck = (type, { top, right, bottom, left }) => {
    if (type === "padding") {
      setAttributes({
        padding: {
          top: top || "0",
          right: right || "0",
          bottom: bottom || "0",
          left: left || "0",
        },
      });
    }
  };
  const handleChangeWidth = (index) => {
    for (let i = 0; i < isPrimary.length; i++) {
      isPrimary[i]["status"] = false;
    }
    console.log(isPrimary);
    isPrimary[index]["status"] = true;
    setAttributes({ width: isPrimary[index]["value"] });
    let newData = [...isPrimary];
    setIsPrimary(newData);
  };
  return (
    <>
      <InspectorControls>
        <PanelBody title="Border Radius" initialOpen={true}>
          <RangeControl
            help="Change card border to circle"
            initialPosition={0}
            max={100}
            min={0}
            onChange={(borderRadius) => setAttributes({ borderRadius })}
            currentInput={0}
          />
        </PanelBody>
        <PanelBody title="Padding" initialOpen={true}>
          <PanelRow>
            <BoxControl
              onChange={(value) => handleCheck("padding", value)}
              defaultValues={{
                top: "0px",
                right: "0px",
                bottom: "0px",
                left: "0px",
              }}
            />
          </PanelRow>
        </PanelBody>
        <PanelBody title="Width">
          <ButtonGroup>
            {isPrimary.map((val, index) => {
              let isSelect = val.status ? `primary` : `secondary`;
              return (
                <Button
                  variant={isSelect}
                  onClick={() => handleChangeWidth(index)}
                >
                  {val.value}
                </Button>
              );
            })}
          </ButtonGroup>
        </PanelBody>
      </InspectorControls>

      <div {...blockProps}>
        <input
          type={typeInput}
          value={value}
          placeholder={placeholder}
          style={{
            "--border-color": colorProps.style.backgroundColor,
            "--text-input": colorProps.style.color,
            borderRadius: `${borderRadius}px`,
            padding: `${padding.top} ${padding.right} ${padding.bottom} ${padding.left}`,
            width: width,
          }}
          onChange={(e)=>{setAttributes({ value:e.target.value })}}
        />
      </div>
    </>
  );
}

function SaveComponent({ attributes = {} }) {
  const { typeInput, placeholder, borderRadius, padding, width } = attributes;
  const blockProps = useBlockProps.save({ className: "input-control" });
  const colorProps = getColorClassesAndStyles(attributes);
  return (
    <>
      <div {...blockProps}>
        <input
          type={typeInput}
          value=""
          placeholder={placeholder}
          style={{
            "--border-color": colorProps.style.backgroundColor,
            "--text-input": colorProps.style.color,
            borderRadius: `${borderRadius}px`,
            padding: `${padding.top} ${padding.right} ${padding.bottom} ${padding.left}`, width: width
          }}
        />
      </div>
    </>
  );
}
