import { registerBlockType } from "@wordpress/blocks";
import {
  InspectorControls,
  MediaUpload,
  MediaUploadCheck,
  __experimentalImageSizeControl as ImageSizeControl,
  BlockControls,
  AlignmentToolbar,
} from "@wordpress/block-editor";
import {
  Button,
  PanelBody,
  PanelRow,
  ResizableBox,
  AnglePickerControl,
  ToolbarGroup,
} from "@wordpress/components";
import { Icon, plusCircle } from "@wordpress/icons";
import { LollypopLogo } from "../../../assets/images/logo.js";

registerBlockType("block-themes/media-image", {
  title: "Media",
  category: "lollypop",
  icon: LollypopLogo,
  attributes: {
    image: { type: "object", url: "" },
    width: {
      type: "number",
    },
    height: {
      type: "number",
    },
    initialWidth: { type: "number" },
    initialHeight: { type: "number" },
    degree: { type: "number", default: 0 },
    customClassName: { type: "string", default: "" },
    customStyle: { type: "object" },
    align: {
      type: "string",
      default: "center",
    },
  },
  supports: {
    className: false,
  },
  edit: EditComponent,
  save: SaveComponent,
});

function EditComponent(props) {
  const { attributes = {}, setAttributes = () => {} } = props;

  const {
    image: { url = "" } = {},
    height,
    width,
    initialWidth,
    initialHeight,
    degree,
    customClassName,
    customStyle,
    align,
  } = attributes;

  const MediaButton = (open) => {
    return <Icon onClick={open} className="icon" icon={plusCircle} />;
  };

  const handleFileSelect = (file) => {
    setAttributes({
      image: { ...file },
      initialWidth: file.width,
      initialHeight: file.height,
      width: file.width,
      height: file.height,
    });
  };

  const handleChangeSize = (width, height) => {
    if (width === undefined) {
      setAttributes({ height });
    }
    if (height === undefined) {
      setAttributes({ width });
    }
    if (width && height) {
      setAttributes({ width, height });
    }
  };

  return (
    <div className={`d-flex justify-content-${align} ${customClassName}`}>
      <div style={{ ...customStyle }} className={`imageBlock`}>
        {url ? (
          <img
            style={{
              transform: `rotate(${degree}deg)`,
              width: width || "100%",
              height: height || "100%",
            }}
            className="image"
            src={url}
            alt="image"
          />
        ) : (
          <div
            style={{ height: height || "50px", width: width || "50px" }}
            className="addImageBox position-relative"
          >
            <MediaUploadCheck>
              <MediaUpload
                onSelect={(file) => handleFileSelect(file)}
                value={1}
                render={({ open }) => MediaButton(open)}
              />
            </MediaUploadCheck>
          </div>
        )}

        <InspectorControls>
          <PanelBody title="Customize Image" initialOpen={true}>
            <PanelRow>
              <MediaUploadCheck>
                <MediaUpload
                  onSelect={handleFileSelect}
                  value={1}
                  render={({ open }) => {
                    return (
                      <Button variant="primary" onClick={open}>
                        Change Media
                      </Button>
                    );
                  }}
                />
              </MediaUploadCheck>
            </PanelRow>
          </PanelBody>
          <PanelBody>
            <PanelRow>
              <ImageSizeControl
                width={width}
                height={height}
                imageWidth={initialWidth}
                imageHeight={initialHeight}
                onChange={({ width, height }) =>
                  handleChangeSize(width, height)
                }
              />
            </PanelRow>
          </PanelBody>
          <PanelBody title="Image rotate">
            <PanelRow>
              <AnglePickerControl
                onChange={(degree) => setAttributes({ degree })}
                value={degree}
              />
            </PanelRow>
          </PanelBody>
        </InspectorControls>
        <BlockControls>
          <ToolbarGroup>
            <AlignmentToolbar
              value={align}
              alignmentControls={[
                {
                  title: "Left",
                  icon: "editor-alignleft",
                  align: "start",
                },
                {
                  title: "Center",
                  icon: "editor-aligncenter",
                  align: "center",
                },
                {
                  title: "Right",
                  icon: "editor-alignright",
                  align: "end",
                },
              ]}
              onChange={(align) => setAttributes({ align })}
            />
          </ToolbarGroup>
        </BlockControls>
      </div>
    </div>
  );
}

function SaveComponent({ attributes }) {
  const {
    image: { url = "" } = {},
    height,
    width,
    degree,
    customClassName,
    customStyle,
    align,
  } = attributes;

  return (
    <div className={`d-flex justify-content-${align} `}>
      <div style={{ ...customStyle }} className={`imageBlock`}>
        <img
          style={{
            transform: `rotate(${degree}deg)`,
            height: height || "100%",
            width: width || "100%",
          }}
          className="image"
          src={url}
          alt="image"
        />
      </div>
    </div>
  );
}
